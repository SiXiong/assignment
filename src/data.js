var jobs = [
    {
        name: "Software Developers & Programmers",
        vacancies: 590,
        maxSalary: 100,
        minSalary: 72,
        skills: [
            "computer software and systems",
            "programming languages and techniques",
            "software development processes such as Agile",
            "confidentiality, data security and data protection issues."
        ],
        description: "Software developers and programmers develop and maintain computer software, " +
        "websites and software applications (apps)."
    }, {
        name: "Database & Systems Administrators",
        vacancies: 74,
        maxSalary: 90,
        minSalary: 66,
        skills: [
            "a range of database technologies and operating systems",
            "new developments in databases and security systems",
            "computer and database principles and protocols"
        ],
        description: "Database & systems administrators develop, maintain and administer computer operating systems, " +
        "database management systems, and security policies and procedures."
    }, {
        name: "Help Desk & IT Support",
        vacancies: 143,
        maxSalary: 65,
        minSalary: 46,
        skills: [
            "computer hardware, software, networks and websites",
            "the latest developments in information technology."
        ],
        description: "Information technology (IT) helpdesk/support technicians set up computer and other IT " +
        "equipment and help prevent, identify and fix problems with IT hardware and software."
    }, {
        name: "Data Analyst",
        vacancies: 270,
        maxSalary: 128,
        minSalary: 69,
        skills: [
            "data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R",
            "data analysis, mapping and modelling techniques",
            "analytical techniques such as data mining"
        ],
        description: "Data analysts identify and communicate trends in data using statistics and " +
        "specialised software to help organisations achieve their business aims."
    }, {
        name: "Test Analyst",
        vacancies: 127,
        maxSalary: 98,
        minSalary: 70,
        skills: [
            "programming methods and technology",
            "computer software and systems",
            "project management"
        ],
        description: "Test analysts design and carry out testing processes for new and upgraded " +
        "computer software and systems, analyse the results, and identify and report problems."
    }, {
        name: "Project Management",
        vacancies: 188,
        maxSalary: 190,
        minSalary: 110,
        skills: [
            "principles of project management",
            "approaches and techniques such as Kanban and continuous testing",
            "how to handle software development issues",
            "common web technologies used by the scrum team."
        ],
        description: "Project managers use various methods to keep project teams on track. They also " +
        "help remove obstacles to progress."
    }
];


var jobLen, text, i;
jobLen = jobs.length;

text = "";
text += "<table class = 'table mt-5 table-border'>";
// the head of the table
text += "<tr class = 'tableHead1'><th colspan='4'>" + "Jobs in ICT - Click to show more information" + "</th></tr>";
text += "<tr class = 'tableHead2'>" +
    "<td>" + "Role" + "</td>" +
    "<td>" + "Vacancies" + "</td>" +
    "<td>" + "Salary Max" + "</td>" +
    "<td>" + "Salary Min" + "</td>" +
    "</tr>";

// the body of the table
for (i = 0; i < jobLen; i++) {
    text += "<tr >";
    text += "<td class='name'>" + jobs[i].name + "</td>";
    text += "<td>" + jobs[i].vacancies + "</td>";
    text += "<td>" + jobs[i].maxSalary + "</td>";
    text += "<td>" + jobs[i].minSalary + "</td>";
    text += "</tr>"
}

// the foot of the table
// to get the total vacancies and the average numbers of max/min salaries
var totalVacancies, max, min, aveForMax, aveForMin;

totalVacancies = 0;
max = 0;
min = 0;

for (i = 0; i < jobLen; i++) {
    totalVacancies += jobs[i].vacancies;
    max += jobs[i].maxSalary;
    min += jobs[i].minSalary;
}
aveForMax = Math.floor(max / jobLen);
aveForMin = Math.floor(min / jobLen);

text += "<tr>";
text += "<td>" + "" + "</td>";
text += "<td>" + "Total: " + totalVacancies + "</td>";
text += "<td>" + "Average: " + aveForMax + "</td>";
text += "<td>" + "Average: " + aveForMin + "</td>";

text += "</table>";
document.getElementById("table").innerHTML = text;


var extraInfo = "";
// Extra information will be displayed once the page opened
document.getElementsByTagName("body")[0].onload = function () {
    extraInfo += "<div class = 'extraInfo my-4 container'>";

    extraInfo += "<h4>";
    extraInfo += jobs[3].name;
    extraInfo += "</h4>";

    extraInfo += "<p>";
    extraInfo += jobs[3].description;
    extraInfo += "</p>";

    extraInfo += "<ul>";
    for (var p = 0; p < jobs[3].skills.length; p++) {
        extraInfo += "<li>";
        extraInfo += jobs[3].skills[p];
        extraInfo += "</li>";
    }
    extraInfo += "</ul>";
    extraInfo += "<img class=\"image mt-3\" src=\"images/" + 3 + ".jpg\" alt=\"You may insert an image\"/>";
    extraInfo += "</div>";

    document.getElementById("extraInformation").innerHTML = extraInfo;
};

// Show the extra information once clicking a row of the table
for (i = 0; i < jobLen; i++) {
    (function (i) {
        document.getElementsByClassName("name")[i].onclick = function () {

            extraInfo = "";
            // It has a container, but 'row'(class) was used here for good looking
            extraInfo += "<div class = 'extraInfo my-4 container'>";
            extraInfo += "<h4>";
            extraInfo += jobs[i].name;
            extraInfo += "</h4>";

            extraInfo += "<p>";
            extraInfo += jobs[i].description;
            extraInfo += "</p>";

            // load the information about skills in a List
            extraInfo += "<ul>";
            for (var p = 0; p < jobs[i].skills.length; p++) {
                extraInfo += "<li>";
                extraInfo += jobs[i].skills[p];
                extraInfo += "</li>";
            }
            extraInfo += "</ul>";
            // load the image
            extraInfo += "<img class=\"image mt-3\" src=\"images/" + i + ".jpg\" alt=\"You may insert an image\"/>";
            extraInfo += "</div>";
            document.getElementById("extraInformation").innerHTML = extraInfo;
        }
    })(i);
}